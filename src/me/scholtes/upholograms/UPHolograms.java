package me.scholtes.upholograms;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.plugin.java.JavaPlugin;

import me.scholtes.upholograms.commands.HologramCommand;
import me.scholtes.upholograms.events.ArmorStandInteract;
import me.scholtes.upholograms.objects.Hologram;
import me.scholtes.upholograms.utils.FileSystem;

public class UPHolograms extends JavaPlugin {

	private static UPHolograms instance;
	private List<Hologram> holograms = new ArrayList<Hologram>();

	public void onEnable() {
		instance = this;
		
		getServer().getPluginManager().registerEvents(new ArmorStandInteract(), this);
		getCommand("holo").setExecutor(new HologramCommand(this));
		
		for (File groupfile : new File(getDataFolder() + "").listFiles()) {
			FileSystem file = new FileSystem(groupfile);
			file.load();
			Location location = new Location(Bukkit.getWorld(file.getString("hologram.location.world")), file.getDouble("hologram.location.x"), file.getDouble("hologram.location.y"), file.getDouble("hologram.location.z"));
			List<String> lines = file.getStringList("hologram.lines");
			Hologram holo = new Hologram(file.getName().replace(".yml", ""), location, lines.get(0));
			for (int i = 1; i < lines.size(); i++) holo.addLine(lines.get(i));
			holograms.add(holo);
		}
	}
	
	public void onDisable() {
		for (Hologram holo : holograms) {
			holo.save();
			holo.clearHolo();
		}
	}

	public static UPHolograms getInstance() {
		return instance;
	}

	public List<Hologram> getHolograms() {
		return holograms;
	}

}
