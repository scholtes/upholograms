package me.scholtes.upholograms.utils;

import org.bukkit.ChatColor;

public class Utils {

	public static String color(String text) {
		return ChatColor.translateAlternateColorCodes('&', text);
	}
	
}
