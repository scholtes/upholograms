package me.scholtes.upholograms.utils;

import java.io.File;
import java.util.List;
import java.util.Set;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class FileSystem {
 
    private File folder;
    private File file;
    private YamlConfiguration configuration;
 
    public FileSystem(File file, String filename) {
        this.folder = file;
        this.file = new File(file, filename);
        this.configuration = new YamlConfiguration();
    }
 
    public FileSystem(File file, File file2) {
        this.folder = file;
        this.file = file2;
        this.configuration = new YamlConfiguration();
    }
 
    public FileSystem(File file) {
        this.folder = file.getParentFile();
        this.file = file;
        this.configuration = new YamlConfiguration();
    }
 
    public File getFolder() {
        return folder;
    }
 
    public File getFile() {
        return file;
    }
 
    public boolean exists() {
        if (file.exists()) {
            return true;
        }
        return false;
    }
 
    public String getName() {
        return file.getName();
    }
 
    public Set<String> getKeys() {
        return configuration.getKeys(false);
    }
 
    public boolean create() {
        if (!folder.exists()) {
            folder.mkdirs();
        }
        if (!file.exists()) {
            try {
                file.createNewFile();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }
 
    public File getJFile() {
        return this.file;
    }
 
    public boolean delete() {
        file.delete();
        return true;
    }
 
    public boolean save() {
        try {
            configuration.save(file);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
 
    public boolean load() {
        try {
            configuration.load(file);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            create();
        }
        return false;
    }
 
    public void set(String place, Object object) {
        configuration.set(place, object);
    }
 
    public FileConfiguration getFC() {
        return configuration;
    }
 
    public ConfigurationSection getSection(String place) {
        return configuration.getConfigurationSection(place);
    }
 
    public int getInt(String place) {
        return configuration.getInt(place);
    }
 
    public String getString(String paramString) {
        return configuration.getString(paramString);
    }
 
    public long getLong(String paramString) {
        return configuration.getLong(paramString);
    }
 
    public boolean getBoolean(String paramString) {
        return configuration.getBoolean(paramString);
    }
 
    public List<String> getStringList(String paramString) {
        return configuration.getStringList(paramString);
    }
 
    public float getFloat(String paramString) {
        return (float) getDouble(paramString);
    }
 
    public double getDouble(String paramString) {
        return configuration.getDouble(paramString);
    }
}