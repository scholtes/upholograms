package me.scholtes.upholograms.objects;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;

import me.scholtes.upholograms.utils.FileSystem;
import me.scholtes.upholograms.utils.Utils;

public class Hologram extends BaseHologram {

	Location location;
	private List<String> lines = new ArrayList<String>();
	private List<ArmorStand> armorStands = new ArrayList<ArmorStand>();

	public Hologram(String name, Location location, String text) {
		super(name);
		this.location = location;
		lines.add(text);
		createHolo();
		save();
	}
	
	public List<String> getLines() {
		return lines;
	}

	public void addLine(String text) {
		lines.add(Utils.color(text));

		clearHolo();
		createHolo();
		save();
	}

	public void removeLine(int line) {
		lines.remove(line - 1);

		clearHolo();
		createHolo();
		save();
	}

	public void setLine(int line, String text) {
		lines.set(line - 1, Utils.color(text));
		clearHolo();
		createHolo();
		save();
	}

	public void insertLine(int line, String text) {
		List<String> newlines = new ArrayList<String>();
		for (int i = 0; i < lines.size(); i++) {
			if (i == line - 1) {
				newlines.add(text);
				newlines.add(lines.get(line - 1));
				continue;
			}
			newlines.add(lines.get(i));
        }

		lines = newlines;
		save();
		clearHolo();
		createHolo();
		save();
	}

	public void remove() {
		clearHolo();
		lines.clear();
		getFile().getJFile().delete();
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public void createHolo() {
		ArmorStand armorStand = (ArmorStand) location.getWorld().spawnEntity(location, EntityType.ARMOR_STAND);

		armorStand.setVisible(false);
		armorStand.setCustomNameVisible(true);
		armorStand.setCustomName(Utils.color(lines.get(0)));
		armorStand.setGravity(false);
		armorStand.setCanPickupItems(false);

		armorStands.add(armorStand);

		Location current = location.clone();

		for (int i = 1; i < lines.size(); i++) {
			current = current.add(0.0D, -0.24D, 0.0D);

			ArmorStand line = (ArmorStand) location.getWorld().spawnEntity(current, EntityType.ARMOR_STAND);

			line.setVisible(false);
			line.setCustomNameVisible(true);
			line.setCustomName(Utils.color(lines.get(i)));
			line.setGravity(false);
			line.setCanPickupItems(false);

			armorStands.add(line);
		}
	}

	public void clearHolo() {
		for (ArmorStand as : armorStands) {
			as.remove();
		}
		armorStands.clear();
	}

	public void save() {
		FileSystem holoFile = getFile();
		if (!holoFile.exists())
			holoFile.create();

		holoFile.load();

		holoFile.set("hologram.name", name);
		holoFile.set("hologram.location.x", location.getX());
		holoFile.set("hologram.location.y", location.getY());
		holoFile.set("hologram.location.z", location.getZ());
		holoFile.set("hologram.location.world", location.getWorld().getName());
		holoFile.set("hologram.lines", lines);

		holoFile.save();
	}

	public Hologram create() {
		lines = new ArrayList<String>();
		armorStands = new ArrayList<ArmorStand>();

		save();

		return this;
	}
}
