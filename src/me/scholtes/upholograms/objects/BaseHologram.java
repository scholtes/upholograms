package me.scholtes.upholograms.objects;

import java.io.File;

import me.scholtes.upholograms.UPHolograms;
import me.scholtes.upholograms.utils.FileSystem;

public abstract class BaseHologram {

	protected String name;
	
	public BaseHologram(String name) {
		this.name = name;
	}
	
	public FileSystem getFile() {
		return new FileSystem(new File(UPHolograms.getInstance().getDataFolder(), name + ".yml"));
	}
	
	public String getName() {
		return name;
	}
	
}
