package me.scholtes.upholograms.events;

import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;

public class ArmorStandInteract implements Listener {
	
	@EventHandler
	public void onManipulate(PlayerArmorStandManipulateEvent e) {
		if (!e.getRightClicked().isVisible()) e.setCancelled(true);
	}
	
	@EventHandler
	public void onDamage(EntityDamageEvent e) {
		if (!(e.getEntity() instanceof ArmorStand)) return;
		ArmorStand entity = (ArmorStand) e.getEntity();
		if (e.getEntityType() == EntityType.ARMOR_STAND && !entity.isVisible()) {
			e.setCancelled(true);
		}
	}

}
