package me.scholtes.upholograms.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.scholtes.upholograms.UPHolograms;
import me.scholtes.upholograms.objects.Hologram;
import me.scholtes.upholograms.utils.Utils;

public class HologramCommand implements CommandExecutor {

	UPHolograms plugin;
	
	public HologramCommand(UPHolograms plugin) {
		this.plugin = plugin;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if (!sender.hasPermission("hologram.admin")) {
			sender.sendMessage(Utils.color("&cNo permission."));
			return true;
		}
		if (!(sender instanceof Player)) {
			sender.sendMessage(Utils.color("&cNot a player."));
			return true;
		}
		Player p = (Player) sender;
		if (args.length < 2) {
			helpMessage(p);
			return true;
		}
		if (args[0].equalsIgnoreCase("create")) {
			String name = args[1];
			for (Hologram holo : plugin.getHolograms()) {
				if (holo.getName().equalsIgnoreCase(args[1])) {
					p.sendMessage(Utils.color("&cA hologram with this name already exists!"));
					return true;
				}
			}
			String text = "&fExample Text";
			if (args.length >= 3) {
				text = joinArgs(args);
			}
			Hologram holo = new Hologram(name, p.getLocation().add(0D, -2D, 0D), text);
			plugin.getHolograms().add(holo);
			p.sendMessage(Utils.color("&aSuccessfully&a created the hologram &2" + holo.getName() + "&a!"));
			return true;
		}
		if (args[0].equalsIgnoreCase("remove")) {
			for (Hologram holo : plugin.getHolograms()) {
				if (!holo.getName().equalsIgnoreCase(args[1])) continue;
				holo.remove();
				plugin.getHolograms().remove(holo);
				p.sendMessage(Utils.color("&aSuccessfully&a removed the hologram &2" + holo.getName() + "&a!"));
				return true;
			}
			p.sendMessage(Utils.color("&cNo such hologram exists!"));
			return true;
		}
		if (args[0].equalsIgnoreCase("movehere") || args[0].equalsIgnoreCase("tphere")) {
			for (Hologram holo : plugin.getHolograms()) {
				if (!holo.getName().equalsIgnoreCase(args[1])) continue;
				holo.setLocation(p.getLocation().add(0D, -2D, 0D));
				holo.clearHolo();
				holo.createHolo();
				p.sendMessage(Utils.color("&aSuccessfully&a moved the hologram &2" + holo.getName() + "&a!"));
				return true;
			}
			p.sendMessage(Utils.color("&cNo such hologram exists!"));
			return true;
		}
		if (args[0].equalsIgnoreCase("addline")) {
			if (args.length < 3) {
				helpMessage(p);
				return true;
			}
			for (Hologram holo : plugin.getHolograms()) {
				if (!holo.getName().equalsIgnoreCase(args[1])) continue;
				holo.addLine(joinArgs(args));
				p.sendMessage(Utils.color("&aSuccessfully&a added a line to &2" + holo.getName() + "&a!"));
				return true;
			}
			p.sendMessage(Utils.color("&cNo such hologram exists!"));
			return true;
		}
		if (args[0].equalsIgnoreCase("removeline")) {
			if (args.length < 3) {
				helpMessage(p);
				return true;
			}
			if (!isInteger(args[2])) {
				sender.sendMessage(Utils.color("&cThe line number must be an integer!"));
				return true;
			}
			for (Hologram holo : plugin.getHolograms()) {
				if (!holo.getName().equalsIgnoreCase(args[1])) continue;
				if (holo.getLines().size() == 1) {
					p.sendMessage(Utils.color("&cThe hologram only has one line!"));
					return true;
				}
				holo.removeLine(Integer.parseInt(args[2]));
				p.sendMessage(Utils.color("&aSuccessfully&a removed a line from &2" + holo.getName() + "&a!"));
				return true;
			}
			p.sendMessage(Utils.color("&cNo such hologram exists!"));
			return true;
		}
		if (args[0].equalsIgnoreCase("insertline")) {
			if (args.length < 4) {
				helpMessage(p);
				return true;
			}
			if (!isInteger(args[2])) {
				sender.sendMessage(Utils.color("&cThe line number must be an integer!"));
				return true;
			}
			for (Hologram holo : plugin.getHolograms()) {
				if (!holo.getName().equalsIgnoreCase(args[1])) continue;
				if (holo.getLines().size() < Integer.parseInt(args[2])) {
					p.sendMessage(Utils.color("&cThe hologram only has " + holo.getLines().size() + " lines!"));
					return true;
				}
				holo.insertLine(Integer.parseInt(args[2]), joinArgs(args));
				p.sendMessage(Utils.color("&aSuccessfully&a inserted line into &2" + holo.getName() + "&a!"));
				return true;
			}
			p.sendMessage(Utils.color("&cNo such hologram exists!"));
			return true;
		}
		if (args[0].equalsIgnoreCase("setline")) {
			if (args.length < 4) {
				helpMessage(p);
				return true;
			}
			if (!isInteger(args[2])) {
				sender.sendMessage(Utils.color("&cThe line number must be an integer!"));
				return true;
			}
			for (Hologram holo : plugin.getHolograms()) {
				if (!holo.getName().equalsIgnoreCase(args[1])) continue;
				holo.setLine(Integer.parseInt(args[2]), joinArgs(args));
				p.sendMessage(Utils.color("&aSuccessfully&a inserted line into &2" + holo.getName() + "&a!"));
				return true;
			}
			p.sendMessage(Utils.color("&cNo such hologram exists!"));
			return true;
		}
		return true;
	}
	
	private void helpMessage(Player p) {
		p.sendMessage(Utils.color("&8&m--------------------------"));
		p.sendMessage(Utils.color("&c/holo create <name> <text>"));
		p.sendMessage(Utils.color("&c/holo remove <name>"));
		p.sendMessage(Utils.color("&c/holo addline <name> <text>"));
		p.sendMessage(Utils.color("&c/holo removeline <name> <line>"));
		p.sendMessage(Utils.color("&c/holo insertline <name> <line> <text>"));
		p.sendMessage(Utils.color("&c/holo setline <name> <line> <text>"));
		p.sendMessage(Utils.color("&c/holo movehere <name>"));
		p.sendMessage(Utils.color("&8&m--------------------------"));
	}
	
	private boolean isInteger(String input) {
		try {
			Integer.parseInt(input);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
	
	private String joinArgs(String[] args) {
		String text = "";
		for (int i = 2; i < args.length; i++) {
		    text = args[i] + " ";
		}
		return text.trim();
	}

}
